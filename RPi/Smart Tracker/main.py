# -*- coding: utf-8 -*-
############
## Author : DJoke
## Date : 2018/12/16
## Usage : RPi bluetooth server to controls motors、LED、audio and DWM1000.
############
import RPi.GPIO as GPIO
import bluetooth
import json
import threading
import car
import DW1000RangingAnchor as Anchor
from math import sqrt
from led import *
from audio_player import audio_player
def handledata(recv_data):
    """
    return a list，元素為各個dict字串(只以"}"判斷)，並將不完整的data存於dataBuffer中
    如:["{u'type':u'remote' , u'x':0.0}"]
    """
    global dataBuffer
    data_list = list()
    if(dataBuffer==''):
        i,j = 0,0
        for count in range(recv_data.count('}')):
            j = recv_data[i:].find('}')+1
            data_list.append(recv_data[i:i+j])
            i += j
            # note:字串將存成unicode , 如:{u'type':u'remote' , u'x':0.0}
        dataBuffer = recv_data[i:]
        return data_list
    recv_data = dataBuffer + recv_data
    dataBuffer = str()
    return handledata(recv_data)

def tracking():
    """
    Threading
    終止條件 : 等待藍芽訊息<停止追蹤>
    (1.)按下"追蹤中"按鈕。data = {'type':'track','power':0}
    (2.)按下遙控頁面 。data = {'type':'mode','value':'remote'}
    
    可能在此時需要接收其他訊息:
    (1.)切斷藍芽連接
    (2.)保持距離的修改。data = {'type':'track','distance':50}
    """
    global __stopTracking
    print '[thread]start'
    while True:
        try:
            recv_data = client_socket.recv(1024)
        except:
            print '\n[thread]Connection reset by peer.'
            break
        try:
            datalist = handledata(recv_data)
            for d in datalist:
                data = json.loads(d)
                
                if(data['type'] == 'mode' and data.has_key(u'value')):
                #切換為遙控頁面
                    if(data[u'value'] == u'remote'):
                        __stopTracking = True
                elif(data['type'] == 'track'):
                #按下"追蹤中"按鈕
                    if(data.has_key('power')):
                        if(data['power'] == 0):
                            __stopTracking = True
                    elif(data.has_key('distance')):
                        car._Distance = int(data['distance'])
                        print "[thread]保持距離:",data['distance'],'cm'
        except:
            print '[thread]【recv error】 , recv_data:' , recv_data
            continue
        if(__stopTracking):
            break
    __stopTracking = True
    print '[thread]stop'

def get_position(d1,d2,d3):
    """
    利用最小平方法及三角定位法，求出定位座標(x,y)並回傳
    """
    import numpy as np
    global Anc1,Anc2,Anc3
    A = 2*np.array([[Anc1[0]-Anc2[0],Anc1[1]-Anc2[1]],
                    [Anc2[0]-Anc3[0],Anc2[1]-Anc3[1]],
                    [Anc3[0]-Anc1[0],Anc3[1]-Anc1[1]]])
    b = np.array([  Anc1[0]**2 - Anc2[0]**2 + Anc1[1]**2 - Anc2[1]**2 + d2**2 - d1**2,
                    Anc2[0]**2 - Anc3[0]**2 + Anc2[1]**2 - Anc3[1]**2 + d3**2 - d2**2,
                    Anc3[0]**2 - Anc1[0]**2 + Anc3[1]**2 - Anc1[1]**2 + d1**2 - d3**2,])
    x = np.dot(np.dot(np.linalg.inv(np.dot(A.T,A)),A.T),b)
    return (x[0],x[1])

def StopAll():
    global RunStopState,RunStopCounter
    car.go({'turn':0,'direction':0},{'forward':True,'power':0})
    LED_hide()
    RunStopCounter = 0
    RunStopState = False
    print 'StopAll Function!'

def sound(filename):
    player = audio_player(filename)
    player.speak()

while(True):
    dataBuffer = str()
    car.motorInit()
    __stopTracking = True
    Anc1 = (0,0)
    Anc2 = (-25,-41.7)
    Anc3 = (25,-41.7)
    _DisAver_static = 1.1   #停止中RangeAver大於此值三次則開始運動
    _DisAver_kinetic = 0.93 #運動中RangeAver小於此值三次則靜止
    _DisAver_max = 1.3
    _DisAver_min = 0.6
    RunStopCounter = 0
    RunStopState = False    #True:運動中 ; False:靜止
    beep_file = 'mp3/beep.mp3'
    start_track_file = 'mp3/start_track.mp3'
    remoting_mode_file = 'mp3/remoting_mode.mp3'
    tracking_mode_file = 'mp3/tracking_mode.mp3'
    ## Bluetooth Server
    server_socket=bluetooth.BluetoothSocket( bluetooth.RFCOMM )
    port = 1
    server_socket.bind(("",port))
    server_socket.listen(1)
    try:
        print 'waiting for bt client...'
        client_socket,address = server_socket.accept()  # Block
    except:
        print 'bluetooth server fin.'
        break
    print "Accepted connection from ",address
    '''
    client_socket.send('hihi')
    '''
    try:
        while True:
            try:
                recv_data = client_socket.recv(1024)
            except:
                print '\nConnection reset by peer.'
                break
            
            try:
                datalist = handledata(recv_data)
                #print datalist
            except:
                print '【recv error】 , recv_data:' , recv_data
                continue
                
            for d in datalist:
                print d
                data = json.loads(d)
                if(data['type'] == 'disconnect'):
                    raise StandardError('Ready To listen next client.')
                    break
                elif(data['type'] == 'language'):
                    if(data.has_key(u'value')):
                        print '語言:',{u'eng':'English',u'cht':'中文'}[data[u'value']]
                        if(data[u'value']==u'eng'):
                            start_track_file = 'mp3/start_track.mp3'
                            remoting_mode_file = 'mp3/remoting_mode.mp3'
                            tracking_mode_file = 'mp3/tracking_mode.mp3'
                        elif(data[u'value']==u'cht'):
                            start_track_file = 'mp3/start_track_cht.mp3'
                            remoting_mode_file = 'mp3/remoting_mode_cht.mp3'
                            tracking_mode_file = 'mp3/tracking_mode_cht.mp3'
                    else:
                        print '語言: Error'
                elif(data['type'] == 'mode'):
                    if(data.has_key(u'value')):
                        print '轉換頁面:',{u'remote':'遙控',u'track':'跟蹤'}[data[u'value']]
                        if(data[u'value']==u'remote'):
                            car.changeSpd(1)
                            threading.Thread(target = sound,args=(remoting_mode_file,)).start()
                        elif(data[u'value']==u'track'):
                            car.changeSpd(2)
                            threading.Thread(target = sound,args=(tracking_mode_file,)).start()
                    else:
                        print '轉換頁面: Error'
                elif(data['type'] == 'remote'):
                #遙控頁面
                    if(data.has_key('x') and data.has_key('y')):
                    #搖桿訊息
                        x = data['x'] * 100.0
                        y = data['y'] * 100.0
                        #print '\nrecv remote info. x:' + str(x) + ' y:' + str(y)
                        if(x>=-100 and x<=100 and y>=-100 and y<=100):
                            car.go(car.get_direction(x,y),car.get_power(x,y))
                    if(data.has_key('speed')):
                    #速度調整
                        print "Speed:",data['speed']
                        car.changeSpd(data['speed'])
                    if(data.has_key('speaker')):
                    #喇叭
                        print "Speaker:",data['speaker']
                        if(data['speaker']):
                            player = threading.Thread(target = sound,args=(beep_file,))
                            player.start()
                elif(data['type'] == 'track'):
                #跟蹤頁面
                    if(data.has_key('power')):
                    #開始跟蹤與否
                        print "開始跟蹤:",{1:'是',0:'否'}[data['power']]
                        if(data['power']):
                            __stopTracking = False
                            thr_track = threading.Thread(target = tracking)
                            thr_track.start()
                            threading.Thread(target = sound,args=(start_track_file,)).start()
                            Anchor.AnchorSetup()
                            on_start = 3
                            while(True):
                                try:
                                    if(__stopTracking):
                                        StopAll()
                                        Anchor.AnchorReset()
                                        print 'Stop Tracking!'
                                        break
                                    RangeDict = Anchor.loop()
                                    if(not RangeDict):#未完成定位時的通訊將回傳None
                                        continue
                                    if(on_start):
                                        on_start -= 1
                                        if(not on_start):
                                            StopAll()
                                        continue
                                    s = ('Out of Range:') + ('0 ' if(RangeDict[0]<0) else '') + ('1 ' if(RangeDict[1]<0) else '') + ('2 ' if(RangeDict[2]<0) else '')
                                    if(len(s)>13):
                                        print s
                                        StopAll()
                                    else:
                                        #x,y = get_position(RangeDict[0]*100,RangeDict[1]*100,RangeDict[2]*100)
                                        RangeAver = (RangeDict[0]+RangeDict[1]+RangeDict[2])/3
                                        diff = RangeDict[1]-RangeDict[2]
                                        if(RangeAver>=0.7):
                                            forward = False if(RangeDict[0]>max(RangeDict[1],RangeDict[2])) else True
                                            if(RangeDict[0]>max(RangeDict[1],RangeDict[2])):
                                                print 'backword'
                                        else:
                                            forward = True
                                        print 'diff=',diff,'\tRangeAver=',RangeAver
                                        if(RunStopState):   #運動中
                                            if(RangeAver<_DisAver_kinetic):
                                                RunStopCounter += 1
                                                if(RangeAver<_DisAver_min):
                                                    RunStopCounter += 2
                                            else:
                                                RunStopCounter = 0
                                            if(RunStopCounter>=3):
                                                StopAll()
                                            else:
                                                d,p = car.get_track_info(RangeDict[0],RangeDict[1],RangeDict[2],RangeAver,forward)
                                                if(forward):
                                                    LED_show(d['direction'])
                                                else:
                                                    if(d['direction']==1):
                                                        LED_show(2)
                                                    else:
                                                        LED_show(1)
                                                car.go(d,p)
                                        else:               #靜止中
                                            if(RangeAver>_DisAver_static):
                                                RunStopCounter += 1
                                            else:
                                                RunStopCounter = 0
                                            if(RunStopCounter>=3 or RangeAver>_DisAver_max):
                                                RunStopState = not RunStopState
                                                RunStopCounter = 0
                                except Exception as e:
                                    __stopTracking = True
                                    StopAll()
                                    print 'Stop Tracking!'
                                    print 'Exception:',e
                                    break
                            thr_track.join()
                            
                    if(data.has_key('distance')):
                    #調整保持距離
                        car._Distance = int(data['distance'])
                        if(car._Distance==50):
                            _DisAver_max = 1.3
                            _DisAver_static = 1.1   #停止中RangeAver大於此值三次則開始運動
                            _DisAver_kinetic = 0.93 #運動中RangeAver小於此值三次則靜止
                            _DisAver_min = 0.7
                        elif(car._Distance==100):
                            _DisAver_max = 1.55
                            _DisAver_static = 1.4   #停止中RangeAver大於此值三次則開始運動
                            _DisAver_kinetic = 1.25 #運動中RangeAver小於此值三次則靜止
                            _DisAver_min = 0.95
                        elif(car._Distance==150):
                            _DisAver_max = 1.75
                            _DisAver_static = 1.58  #停止中RangeAver大於此值三次則開始運動
                            _DisAver_kinetic = 1.46 #運動中RangeAver小於此值三次則靜止
                            _DisAver_min = 1.2
                        print "保持距離:",data['distance'],'cm'
    except KeyboardInterrupt:
        car.motorClose()
        break
    except StandardError as e:
        print 'Client disconnect.'
        print e
    except Exception as e:
        print '[Exception]:',e
        break