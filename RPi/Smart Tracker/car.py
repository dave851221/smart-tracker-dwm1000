# -*- coding: utf-8 -*-
############
## Author : DJoke
## Date : 2018/12/16
## Usage : RPi motors API
############
import RPi.GPIO as GPIO
GPIO.setwarnings(False)
GPIO.setmode(GPIO.BCM)    # GPIO.BOARD(電路板上接腳號碼) or GPIO.BCM(GPIO後面的號碼)
'''BOARD'''
"""
IN1 = 29
IN2 = 31
IN3 = 32
IN4 = 33
ENA_Pin = 37
ENB_Pin = 38
"""
'''BCM'''
IN1 = 5
IN2 = 6
IN3 = 12
IN4 = 13
ENA_Pin = 26
ENB_Pin = 20
Relay_Pin = 18
GPIO.setup(IN1,GPIO.OUT)        #IN1
GPIO.setup(IN2,GPIO.OUT)        #IN2
GPIO.setup(IN3,GPIO.OUT)        #IN3
GPIO.setup(IN4,GPIO.OUT)        #IN4
GPIO.setup(ENA_Pin,GPIO.OUT)    #ENA
GPIO.setup(ENB_Pin,GPIO.OUT)    #ENB
GPIO.setup(Relay_Pin,GPIO.OUT)  #繼電器
ENA = GPIO.PWM(ENA_Pin,600)     # frequency:(使用L298N , 頻率為500Hz或以上)
ENB = GPIO.PWM(ENB_Pin,600)     # frequency:(使用L298N , 頻率為500Hz或以上)
ENA.start(0)
ENB.start(0)
_Speed = 0.66   # 遙控模式的速度
_Distance = 50  # 保持距離<cm>(default=50) value:(50,100,150)
_turning = False
_current_direction = {}
_current_power = {}

def motorInit():
    """
    初始化馬達模組的各個腳位
    """
    for i in [IN1,IN2,IN3,IN4]:
        GPIO.output(i,False)
    return

def motorClose():
    """
    清除腳位
    """
    GPIO.setwarnings(False)
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(IN1,GPIO.OUT)    #IN1
    GPIO.setup(IN2,GPIO.OUT)    #IN2
    GPIO.setup(IN3,GPIO.OUT)    #IN3
    GPIO.setup(IN4,GPIO.OUT)    #IN4
    for i in [IN1,IN2,IN3,IN4]:
        GPIO.output(i,False)
    ENA.stop()
    ENB.stop()
    GPIO.cleanup()

def motorControl(m,d):
    """
    m : motorA or motorB  ;  d : forward or backward
    控制馬達驅動模組(L298N)的IN1~IN4
    """
    GPIO.setmode(GPIO.BCM)
    if(m=='A'):
        if(d=='f'):
            GPIO.output(IN1, True)
            GPIO.output(IN2, False)
        elif(d=='b'):
            GPIO.output(IN1, False)
            GPIO.output(IN2, True)
    elif(m=='B'):
        if(d=='f'):
            GPIO.output(IN3, True)
            GPIO.output(IN4, False)
        elif(d=='b'):
            GPIO.output(IN3, False)
            GPIO.output(IN4, True)
    return

def changeSpd(value):
    global _Speed
    if(value==0):
        _Speed = 0.44
    elif(value==1):
        _Speed = 0.66
    elif(value==2):
        _Speed = 0.77
    else:
        print 'changeSpd func err , 0 ~ 2 expected.'
    return

def get_direction(x,y):
    """
    回傳dict，包含轉向(direction)及幅度(turn)
    """
    import math
    
    # 轉向
    if(-30<x<30):
        direction = 0
    elif(x<0):
        direction = 1
    else:
        direction = 2
    
    # 幅度
    if(direction!=0):   #x=>30 or x<=-30
        
        angle = abs(math.atan(x/y)/(2*math.pi)*360)
        if(-40<y<0):
            angle = 100
        turn = angle
    else:
        turn = 0
    #print '(x,y):',(x,y),'\tturn:',turn
    #direction(轉向):接收判斷轉向之軸的數值，結果輸出0,1,2分別代表直走,左轉,右轉。
    #turn(幅度):0~100，代表轉彎的幅度(0~100%)。
    return {'direction':direction,'turn':turn}
    
def get_power(x,y):
    """
    回傳dict，包含方向(forward)及強度(power)
    """
    import math
    power = math.sqrt(x*x + y*y)
    power = 100 if(power>100) else power
    if(y<-40 or (y<0 and -30<x<30)):
        forward = False
    else:
        forward = True
    #power(馬達強度):0~100
    #forward(方向):True為向前，False為後退
    return {'power':power,'forward':forward}
    
def get_track_info(r0,r1,r2,RangeAver,forward):
    import math
    global _turning
    have_package = True
    diff = abs(r1-r2)
    if(forward):
        '''Direction'''
        if(_turning):
            max_diff = 0.34
        else:
            max_diff = 0.27
        if(diff<max_diff):  #站著測的參數約0.31 , 運動中的參數約0.37
            direction = 0
            _turning = False
        #elif(x <= -straight_tolerance):
        elif(r1<r2):
            direction = 1
            _turning = True
        else:
            direction = 2
            _turning = True
        #turn = 2**(angle/15)-4 if(30<= angle <=90) else 0
        
        '''turn and power'''
        #if(RangeAver<2.6):  #距離適中
        if(True):
            power = 43 if have_package else 33
            if(diff<0.4):
                power += 6 if have_package else 0
                turn = 23
            elif(diff<0.53):
                power += 11 if have_package else 2
                turn = 48
            elif(diff<0.65):
                power += 13 if have_package else 3
                turn = 58
            elif(diff<0.7):
                power += 14 if have_package else 5
                turn = 63
            elif(diff<0.8):
                power += 18 if have_package else 5
                turn = 80
            else:
                power += 23 if have_package else 5
                turn = 95
        '''
        else:               #距離過遠
            turn = 0
            power = 0
        '''
        if(direction==0):
            turn = 0
            power = 56 if have_package else 35
    else:
        
        '''turn and power'''
        turn = 100
        power = 72 if have_package else 46
        '''Direction'''
        if(r1>r2):
            direction = 1
            power += 5
        else:
            direction = 2
        
        
    return {'direction':direction,'turn':turn} , {'power':power,'forward':forward}

def go(Direction,Power):
    """
    Direction(dict) => keys : direction , turn
    Power(dict) => keys : forward , power
    """
    global _current_direction , _current_power
    if(_current_direction==Direction and _current_power==Power):
        print 'same go.'
        return
    _current_direction = Direction
    _current_power = Power
    if(Power['power']!=0):
        msg = {True:'向前',False:'向後'}[Power['forward']] + {0:'直走',1:'左轉',2:'右轉'}[Direction['direction']]
        msg += '\n強度:' + str(Power['power']) + '% 幅度:' + str(Direction['turn']) + '%\n'
        print msg
    if(Power['forward']):
        motorControl('A','f')
        motorControl('B','f')
    else:
        motorControl('A','b')
        motorControl('B','b')
    if(Direction['direction']==0):  #直走
        GPIO.output(Relay_Pin,True)
        ENA.ChangeDutyCycle(Power['power']*_Speed)
        ENB.ChangeDutyCycle(Power['power']*_Speed)
    elif(Direction['direction']==1):#左轉
        GPIO.output(Relay_Pin,False)
        ENA.ChangeDutyCycle(Power['power']*_Speed*(100-Direction['turn'])/100)
        ENB.ChangeDutyCycle(Power['power']*_Speed)
    elif(Direction['direction']==2):#右轉
        GPIO.output(Relay_Pin,False)
        ENA.ChangeDutyCycle(Power['power']*_Speed)
        ENB.ChangeDutyCycle(Power['power']*_Speed*(100-Direction['turn'])/100)