# -*- coding: utf-8 -*-
"""
This python script is used to configure the DW1000 chip as an anchor for ranging functionalities. It must be used in conjunction with the RangingTAG script.
It requires the following modules: DW1000, DW1000Constants and monotonic.
"""
import RPi.GPIO as GPIO
import DW1000
import monotonic
import DW1000Constants as C
from led import *
GPIO.setwarnings(False)

lastActivity = 0
expectedMsgId = C.POLL
protocolFailed = False
sentAck = False
receivedAck = False
LEN_DATA = 16
data = [0] * LEN_DATA
timePollAckSentTS = 0
timePollAckReceivedTS = 0
timePollReceivedTS = 0
timeRangeReceivedTS = 0
timePollSentTS = 0
timeRangeSentTS = 0
timeComputedRangeTS = 0
REPLY_DELAY_TIME_US = 7000

PIN_SS = [25,7,16]

Address  = list()
RangeDict = {0:None , 1:None , 2:None}
RangeIndex = 0
def millis():
    """
    This function returns the value (in milliseconds) of a clock which never goes backwards. It detects the inactivity of the chip and
    is used to avoid having the chip stuck in an undesirable state.
    """    
    return int(round(monotonic.monotonic() * C.MILLISECONDS))


def handleSent():
    """
    This is a callback called from the module's interrupt handler when a transmission was successful. 
    It sets the sentAck variable as True so the loop can continue.
    """            
    global sentAck
    sentAck = True


def handleReceived():
    """
    This is a callback called from the module's interrupt handler when a reception was successful. 
    It sets the received receivedAck as True so the loop can continue.
    """       
    global receivedAck
    receivedAck = True


def noteActivity():
    """
    This function records the time of the last activity so we can know if the device is inactive or not.
    """        
    global lastActivity
    lastActivity = millis()


def resetInactive():
    """
    This function restarts the default polling operation when the device is deemed inactive.
    """    
    global expectedMsgId
    #print("reset inactive")
    expectedMsgId = C.POLL
    receiver()
    noteActivity()


def transmitPollAck():
    """
    This function sends the polling acknowledge message which is used to confirm the reception of the polling message. 
    """        
    global data
    DW1000.newTransmit()
    data[0] = C.POLL_ACK
    DW1000.setDelay(REPLY_DELAY_TIME_US, C.MICROSECONDS)
    DW1000.setData(data, LEN_DATA)
    DW1000.startTransmit()


def transmitRangeAcknowledge():
    """
    This functions sends the range acknowledge message which tells the tag that the ranging function was successful and another ranging transmission can begin.
    """
    global data
    DW1000.newTransmit()
    data[0] = C.RANGE_REPORT
    DW1000.setData(data, LEN_DATA)
    DW1000.startTransmit()


def transmitRangeFailed():
    """
    This functions sends the range failed message which tells the tag that the ranging function has failed and to start another ranging transmission.
    """    
    global data
    DW1000.newTransmit()
    data[0] = C.RANGE_FAILED
    DW1000.setData(data, LEN_DATA)
    DW1000.startTransmit()


def receiver():
    """
    This function configures the chip to prepare for a message reception.
    """    
    global data
    DW1000.newReceive()
    DW1000.receivePermanently()
    DW1000.startReceive()


def computeRangeAsymmetric():
    """
    This is the function which calculates the timestamp used to determine the range between the devices.
    """
    global timeComputedRangeTS
    round1 = DW1000.wrapTimestamp(timePollAckReceivedTS - timePollSentTS)
    reply1 = DW1000.wrapTimestamp(timePollAckSentTS - timePollReceivedTS)
    round2 = DW1000.wrapTimestamp(timeRangeReceivedTS - timePollAckSentTS)
    reply2 = DW1000.wrapTimestamp(timeRangeSentTS - timePollAckReceivedTS)
    timeComputedRangeTS = (round1 * round2 - reply1 * reply2) / (round1 + round2 + reply1 + reply2)

def readAddressFile():
    global Address
    from json import loads as load 
    f = open("address.json", "r")
    Address = load(f.read())
    f.close()

def readStandardFile():
    from json import loads as load
    try:
        f = open("standard", "r")
        offset = load(f.read())
        f.close()
    except:
        print 'Standard File Error.'
        return [0,0,0]
    return offset
    
def loop(standardize=False):
    global sentAck, receivedAck, timePollAckSentTS, timePollReceivedTS, timePollSentTS, timePollAckReceivedTS, timeRangeReceivedTS, protocolFailed, data, expectedMsgId, timeRangeSentTS
    global RangeDict , RangeIndex
    if (sentAck == False and receivedAck == False):
        if ((millis() - lastActivity) > 10):
            DW1000.handleInterrupt()
        if ((millis() - lastActivity) > C.RESET_PERIOD):
            resetInactive()
        return

    if sentAck:
        sentAck = False
        msgId = data[0]
        if msgId == C.POLL_ACK:
            timePollAckSentTS = DW1000.getTransmitTimestamp()
            noteActivity()
        if msgId == C.RANGE_REPORT:
            pass
            
    if receivedAck:
        receivedAck = False
        data = DW1000.getData(LEN_DATA)
        msgId = data[0]
        #print('msgId:%d'%msgId)
        #print 'data:',data
        if msgId != expectedMsgId:
            protocolFailed = True
        if msgId == C.POLL:     # 接收到Tag的poll(第一次)
            protocolFailed = False
            timePollReceivedTS = DW1000.getReceiveTimestamp()
            expectedMsgId = C.RANGE
            transmitPollAck()   # 傳送ack至tag
            noteActivity()
        elif msgId == C.RANGE:  # 接收到Tag的range report(第二次)
            timeRangeReceivedTS = DW1000.getReceiveTimestamp()
            expectedMsgId = C.POLL
            if protocolFailed == False:
                timePollSentTS = DW1000.getTimeStamp(data, 1)
                timePollAckReceivedTS = DW1000.getTimeStamp(data, 6)
                timeRangeSentTS = DW1000.getTimeStamp(data, 11)
                computeRangeAsymmetric()
                transmitRangeAcknowledge()
                distance = (timeComputedRangeTS % C.TIME_OVERFLOW) * C.DISTANCE_OF_RADIO
                if distance>1000:
                    distance = -1
                RangeDict[RangeIndex] = round(distance, 2)
                if(not standardize):
                    offset = readStandardFile()
                    if(RangeIndex==0):
                        RangeDict[RangeIndex] += offset[0]
                    elif(RangeIndex==1):
                        RangeDict[RangeIndex] += offset[1]
                    elif(RangeIndex==2):
                        RangeDict[RangeIndex] += offset[2]
                RangeDict[RangeIndex] = round(RangeDict[RangeIndex],2)
                
                RangeIndex = RangeIndex+1 if RangeIndex<2 else 0
                DW1000.reselect(PIN_SS,RangeIndex)
                receivedAck = False
                receiver()
                noteActivity()
                
                if(RangeDict[0]==None or RangeDict[1]==None or RangeDict[2]==None):
                    return None
                return RangeDict
            else:
                transmitRangeFailed()

            noteActivity()
    return

def AnchorSetup():
    from time import sleep
    global PIN_SS , Address
    global RangeIndex
    readAddressFile()
    
    DW1000.registerCallback("handleSent", handleSent)
    DW1000.registerCallback("handleReceived", handleReceived)
    DW1000.begin()
    print("SPI initialized.")
    LED_setup()
    for index in range(3):
        LED_on(index)
    sleep(1)
    for index in range(3):
        LED_off(index)
    success = True
    for index in range(3): #[0,1,2]
        sleep(0.3)
        DW1000.setup(PIN_SS[index])
        uid = DW1000.generalConfiguration(Address[index], C.MODE_LONGDATA_RANGE_ACCURACY)
        if(uid==Address[index]):
            LED_on(index)
        else:
            success = False
        DW1000.setAntennaDelay(C.ANTENNA_DELAY_RASPI)
        print "DW1000 setup : device",index
    print("DW1000 initialized.")
    if(success):
        DW1000.reselect(PIN_SS,RangeIndex)
        receiver()
        noteActivity()
    else:
        print '[Error] AnchorSetup failed.'

def AnchorReset():
    global RangeDict
    RangeDict = {0:None , 1:None , 2:None}
    for i in range(3):
        LED_off(i)

def AnchorClose():
    for i in range(3):
        LED_off(i)
    DW1000.close()